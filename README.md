# Simple Number Game
This is a very simple number game to learn about the syntax and language of Python 3.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPLv3.0](https://choosealicense.com/licenses/gpl-3.0/)

