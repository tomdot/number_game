# TODO: Eval int or str
# TODO: Play again.

import sys
import random


def main():
    """The main loop for the game."""
    number = random.choice(range(1, 10))
    tries = 3

    print("I'm thinking of a number between 1 and 10. You have three guesses.")
    user_guess(number, tries)


def user_guess(number, tries):
    prompt = ">>> "

    while tries:
        guess = input(prompt)

        if int(guess) > 10:
            print(f"Silly. {guess} is higher than 10.")
        else:
            evaluate(guess, number, tries)


def evaluate(guess, number, tries):
    if int(guess) > number:  # Hi-Lo eval
        hi_lo = "lower"
    else:
        hi_lo = "higher"

    if int(guess) == number:  # Right answer
        print(f"Yeah you got it! It was {number}!")
        sys.exit(0)

    if int(guess) != number:  # Wrong answer
        tries -= 1

    if not tries:  # Game over
        print(f"YOU FAILED. THE NUMBER WAS {number}. GAME OVER")
        sys.exit(0)

    # Wrong answer
    print(f"Try again. You have {tries} lives left.")
    print(f"Try guessing a {hi_lo} number next time.")
    user_guess(number, tries)


main()
